![Logo WebEngineering](http://www.webengineering.fr/webengineering/www/webengineering/images/webengineering-logo.png)

# CHALLENGE WEBENGINEERING #

In order to apply to WebEngineering, we want you to realize this small challenge. Our goal is not to have a finished and prodable product but to judge how you develop. Do not waste too much time with "frills".

The subject of this challenge is to create a WebApp to manage a developpers codes' library. 

A code will be identified by :

* A name
* A language
* A developper
* A version number
* A created date
* An updated date

A developper will be identified by : 

* A fullname
* A pseudo
* A mail
* A created date
* An updated date

A code could have likes and comments

### What is this repository for? ###

Evaluate your development skills and philosophy.

### What do you have to do? ###

This challenge will be separated in two parts. You are allowed to do one of two parts. A REST Api which will be used in a JS front application. 

For the database you are free to use MySql, Sqlite or simply a couple of files. If you choose database format please provide initialisation script.
An UML schema will be appreciated ^^

### Step - 1 : Api ###

Please create an api to provide access to data stored in databases or files. 

For api development, WebEngineering uses ApiGility. You can use it if you want or use anything else. 
https://apigility.org/

[Instuctions](/step-1/README.md)

### Step - 2 : Front ###

Build a front application in JS. Feel free to use what you prefer. Your front application has to use the api you build in step 1.
At WebEngineering, we use meteorjs and ReactJS.

[Instuctions](/step-2/README.md)