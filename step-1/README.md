![Logo WebEngineering](http://www.webengineering.fr/webengineering/www/webengineering/images/webengineering-logo.png)

# CHALLENGE WEBENGINEERING - STEP 1 - BUILD AN API

In this exercice we want to evaluate your capacity to create a JSON-RPC api. 
We don't want you to use an unfriendly framework, so you only have to respect the following rules : 

* Use php 7
* The API has to be REST
* Entry and output have to be in JSON RPC

As a SGBD you can use Mysql, PostGre, Mongdb or files. You just have to give it an UML Schema. 

The api has to propose at least the following services :

* list of code
* list of developper
* list of comments
* list of likes

Feel free to propose any other services. The only limitation you have is your imagination!

# Informations #
WebEngineering stack: 

* Php 7
* Zend Framework 3/2
* Apigility
* MySql